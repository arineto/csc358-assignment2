class UnreliableRawSocket(object):
    '''Simulates an unreliable connection by randomly inserting dropped 
    messages and corrupted messages. Only a single bit is ever corrupted.
    Delegates most functionality to a real Python socket.'''

    DROP_RATE = 0
    CORRUPT_RATE = 0

    def __init__(self, port,
                       drop_rate=DROP_RATE, 
                       corrupt_rate=CORRUPT_RATE):
        '''Create an socket bound to the specified port. Optionally set the
        rate at which packets are dropped and corrupted.'''

        import socket
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._sock.bind((socket.gethostname(), port))
        self.set_failrates(drop_rate, corrupt_rate)


    def set_failrates(self, drop_rate, corruption_rate):
        '''Update the failure rates for the specified unreliable socket.'''

        if (0 > drop_rate or drop_rate > 1) or (0 > corruption_rate or corruption_rate > 1):
            raise ValueError("Fault rates must be between 0.0 and 1.0.")
        self._drop_rate = drop_rate
        self._corrupt_rate = corruption_rate


    def sendto(self, data, ip_address):
        '''Send the str data to the str ip_address. 

        Note: The destination port must be embedded in bytes 2-3 of the str 
        data.'''

        import random
        import struct

        try:
            port = struct.Struct("!xx H").unpack(data[: 4])[0]
        except struct.error:
            raise ValueError("data to be sent must contain a header.")

        # A fraction of sent messages are corrupted in transit.
        # Only a single bit is corrupted. 
        if random.random() <= self._corrupt_rate:
            byte = random.randint(0, len(data) - 1)
            bit = 2 ** random.randint(0, 7)
            data = data[: byte] + bytes(chr(data[byte] ^ bit), 'utf-8') + data[byte + 1:]

        # A fraction of sent messages are dropped (lost) in transit.
        if random.random() > self._drop_rate:
            self._sock.sendto(data, (ip_address, port))


    def recvfrom(self, bufferlen):
        '''Receive a message no larger than the specified int size bufferlen
        and return the message and the address of the sender.'''

        # No corrupting or dropping is done; the sender handles it.
        (msg, (addr, port)) = self._sock.recvfrom(bufferlen)
        return (msg, addr)
