from stcp import STCP
import socket

_client_port = 5000
peer_address = socket.gethostname()
peer_port = 5001

if __name__ == "__main__":
    connection = STCP(_client_port)

    connection.connect(peer_address, peer_port)

    while 1:
        msg = input("Please enter a message: ")
        if msg == "" or msg == "CLOSE":
            break

        connection.send(msg)

    connection.close()
        
