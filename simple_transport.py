from unreliable_socket import UnreliableRawSocket as unrsock
from random import randint
import socket
import struct


class SimpleTransportSocket(object):
    '''Implementation of a sample transport protocol.'''

    _header_format = None
    _seq_number = 0
    _ack_number = 0
    _window_size = 1024

    def __init__(self, port, d_rate, c_rate):
        # Creating the unreliable socket. The drop rate and corruption rate are
        # being set here.
        self._sock = unrsock(port, d_rate, c_rate)
        self._port = port


    def set_header_format(self, header_format):
        # Initialize the header format
        self._header_format = struct.Struct(header_format)


    def set_seq_number(self, value):
        # Increments the sequence number
        self._seq_number += value


    def set_ack_number(self, value):
        # Increments the ack number
        self._ack_number += value


    def sendto(self, msg, dest_addr, dest_port, ack=0, syn=0, fyn=0):
        '''Send the str msg to the destination IP addr and port, then waits for an ack if the send message needs an ack.'''
        # Get the bytes of the message
        bytes_msg = bytes(msg, 'utf-8')
        # Set the offset to 24, the size of the header
        offset = 24
        # Creates a pseudo header with the checksum replaced for 0.
        pseudo_header = self.pack_header(dest_port, self._seq_number, self._ack_number, offset, self._window_size, 0, ack, syn, fyn)
        # Calculates the checksum (message and header)
        checksum = ~self.sum_msg(msg, pseudo_header) & 0xffff
        # Creates the header with the correct checksum
        header = self.pack_header(dest_port, self._seq_number, self._ack_number, offset, self._window_size, checksum, ack, syn, fyn)
        
        peer_window_size = None
        try:
            # Send the packet (header plus message), to the peer
            self.sendto_aux(header + bytes_msg, dest_addr)
            # Waits for an ack if it needs an ack
            if ack == 0 or (ack == 1 and syn == 1):
                while 1:
                    (msg, (_server_address, _server_port), peer_window_size) = self.recvfrom()
                    # Stop the function if the ack was received
                    if msg == "ACK" or msg == "SYN-ACK":
                        break
            # Turn off the timeout if the message does not need an ack
            self._sock._sock.settimeout(None)
        except:
            # In case of timeout resend the message
            print("Timeout - Retransmiting message "+str(self._seq_number))
            self.sendto(msg, dest_addr, dest_port, ack, syn, fyn)

        return peer_window_size


    def sendto_aux(self, message, dest_addr):
        # Send the message and set the timeout
        self._sock.sendto(message, dest_addr)
        self._sock._sock.settimeout(1.0)
        

    def recvfrom(self):
        '''Return a (message, address, windows size) tuple containing a str message received 
        from the peer, also check what the message is.'''

        # Extracting the source port while removing the header.
        ((sender_port, seq_number, ack_number, offset, ack, syn, fyn, window_size, checksum), sender_addr, data) = self.recvfrom_aux()

        if syn and ack:
            msg = "SYN-ACK"
            self._seq_number = ack_number
            self._ack_number = seq_number+1
        elif fyn:
            msg = "FYN"
            self._seq_number = ack_number
            self._ack_number = seq_number+1
        elif syn:
            msg = "SYN"
            self._seq_number = randint(1000,9999)
            self._ack_number = seq_number+1
        elif ack:
            msg = "ACK"
            self._seq_number = ack_number
            self._ack_number = seq_number+1
        else:
            # In case of received data
            # Unpack the data
            data = data[self._header_format.size:]
            try:
                # Try to decode the message, if it is not possible, then the message is corrupted
                msg = data.decode('utf-8')
                # Creates a pseudo header the check the correctness of the message and obtain the value of the sum
                pseudo_header = self.pack_header(sender_port, seq_number, ack_number, offset, window_size, 0, ack, syn, fyn)
                check = self.sum_msg(msg, pseudo_header) & 0xffff

                if check + checksum != 65535:
                    # If the sum is not 65535 (0xffff) then the message is corrupted
                    print("Corrupted message detected by checksum.")
                    msg = "Corrupted"
                else:        
                    # In case of the message is not corrupted print it
                    print(msg)
                    # Updates the sequence number and ack number
                    self._seq_number = ack_number
                    self._ack_number = seq_number+len(msg)
                    # Send the ack back
                    ack_header = self.pack_header(sender_port, self._seq_number, self._ack_number, 0, self._window_size, checksum, 1, 0, 0)
                    self._sock.sendto(ack_header, sender_addr)
                    print("ACK for message %d sent" % seq_number)
            except:
                print("Corrupted message detected by checksum.")
                msg = "Corrupted"

        return (msg, (sender_addr, sender_port), self._window_size)


    def recvfrom_aux(self):
        # Receive the data from the peer and return the unpacked header, the sender address and the data.
        (data, sender_addr) = self._sock.recvfrom(self._window_size)
        return (self.unpack_header(data), sender_addr, data)


    def sum_msg(self, msg, pseudo_header):
        """
        Function obtained from http://www.binarytides.com/raw-socket-programming-in-python-linux/ with few modifications.
        It calculates the binary sum of the data and the pseudo header.
        """
        s = 0
        # loop taking 2 characters at a time
        for i in range(0, len(msg)-1, 2):
            w = ord(msg[i]) + (ord(msg[i+1]) << 8 )
            s = s + w
        s = s+sum(pseudo_header)
        s = (s>>16) + (s & 0xffff);
        s = s + (s >> 16);
        return s


    def pack_header(self, dest_port, seq_number, ack_number, offset, window_size, checksum, ack, syn, fyn):
        # Function responsible for packing the header in the correct format
        header = self._header_format.pack(self._port, dest_port, seq_number, ack_number, offset, 0, 0, ack, 0, 0, syn, fyn, window_size, checksum)
        return header


    def unpack_header(self, data):
        # Function responsible for unpacking the header correctly
        # 1 - Source Port (16 bits)                     6 - Reserverd (6 bits)
        # 2 - Destination Port (16 bits)                7 - Control Bits (6 bits): URG, ACK, PSH, RST, SYN, FIN
        # 3 - Sequence Number (32 bits)                 8 - Window (16 bits)
        # 4 - Ack Number (32 bits)                      9 - Checksum (16 bits)
        # 5 - Data offset (4 bits)
        sender_port = self._header_format.unpack(data[: self._header_format.size])[0]
        seq_number = self._header_format.unpack(data[: self._header_format.size])[2]
        ack_number = self._header_format.unpack(data[: self._header_format.size])[3]
        offset = self._header_format.unpack(data[: self._header_format.size])[4]
        ack = self._header_format.unpack(data[: self._header_format.size])[7]
        syn = self._header_format.unpack(data[: self._header_format.size])[10]
        fyn = self._header_format.unpack(data[: self._header_format.size])[11]
        window_size = self._header_format.unpack(data[: self._header_format.size])[12]
        checksum = self._header_format.unpack(data[: self._header_format.size])[13]

        return (sender_port, seq_number, ack_number, offset, ack, syn, fyn, window_size, checksum)