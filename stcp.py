from simple_transport import SimpleTransportSocket
from random import randint
import socket
import struct

_drop_rate = 0.0        # The probability that a segment is lost.
_corrupt_rate = 0.0     # The probability that a segment is corrupted.

class STCP(SimpleTransportSocket):
    '''Implementation of a simple TCP.'''

    def __init__(self, port, d_rate=_drop_rate, c_rate=_corrupt_rate):
        SimpleTransportSocket.__init__(self, port, d_rate, c_rate)

        self.in_connection = False
        self.peer_address = None
        self.peer_port = 0
        self.peer_buffer = 0

        # Create the appropriate header by identifying what fields you need
        # Starting with the full TCP header and stripping out those fields that are not necessary
        # Header Fields:
        # 1 - Source Port (16 bits)                     6 - Reserverd (6 bits)
        # 2 - Destination Port (16 bits)                7 - Control Bits (6 bits): URG, ACK, PSH, RST, SYN, FIN
        # 3 - Sequence Number (32 bits)                 8 - Window Size (16 bits)
        # 4 - Ack Number (32 bits)                      9 - Checksum (16 bits)
        # 5 - Data offset (4 bits)
        # super().set_header_format("!HHLLBBBHH") - Correct format
        super().set_header_format("!HHLLBB??????HH")


    # Methods responsible for creating the connection
    # Exchange initial sequence numbers and windows sizer
    # Don't make assumptions about any number
    # Methods should raise an exception if called with a opened connection

    def connect(self, address, port):
        if not self.in_connection:
            # Set de sequence number
            super().set_seq_number(randint(1000,9999))
            # Send a SYN and wait for a SYN-ACK
            self.peer_buffer = super().sendto("", address, port, 0, 1, 0)
            # Send an ACK
            super().sendto("", address, port, 1, 0, 0)
            # Increment de sequence number
            super().set_seq_number(1)
            # Set the variables
            self.in_connection = True
            self.peer_address = address
            self.peer_port = port
            print("Connected")
        else:
            print("Already connected")


    def accept(self, address, port):
        if not self.in_connection:
            # SYN received, send a SYN-ACK
            super().sendto("", address, port, 1, 1, 0)
            # Set the variables
            self.in_connection = True
            self.peer_address = address
            self.peer_port = port
        else:
            print("Already connected")

    # Methods responsilbe for the data exchange
    # Raise exception if called with connection (not connected, not accepted or closed)

    def send(self, msg):
        # Just call the super function to send the message
        super().sendto(msg, self.peer_address, self.peer_port)


    def recv(self):
        # Call the super function to receive the message
        (msg, (addr, port), window_size) = super().recvfrom()
        # Set the size of the Peer's buffer
        self.peer_buffer = window_size
        if msg == "SYN":
            self.accept(addr, port)
        elif msg == "FYN":
            self.close(True)
        return (msg, (addr, port))

    # Method responsible for closing the connection
    # TCP-style connection tear-down sequence

    def close(self, receive=False):
        if self.in_connection:
            if receive:
                # In case of a received FYN
                # Send an ACK
                super().sendto("", self.peer_address, self.peer_port, 1, 0, 0)
                # Send a FIN-ACK and waits for the ACK
                super().sendto("", self.peer_address, self.peer_port, 1, 0, 1)
            else:
                # In case of try to close the connection
                # Send a FYN-ACK and waits for an ACK
                super().sendto("", self.peer_address, self.peer_port, 1, 0, 1)
                # Waits for the FYN-ACK
                while 1:
                    (msg, (addr, port), window_size) = super().recvfrom()
                    if msg == "FYN":
                        break
                # Send an ACK
                super().sendto("", self.peer_address, self.peer_port, 1, 0, 0)
            # Set the variables
            self.in_connection = False
            self.peer_address = None
            self.peer_port = 0
            print("Connection Closed")
        else:
            print("There is no connection")






























