from stcp import STCP

_port = 5001
_max_msg_size = 1024

if __name__ == "__main__":
    connection = STCP(_port, 0, 0)

    while 1:
        (msg, (addr, port)) = connection.recv()
        #print("Message from (%s, %d) received:\n%s" % (addr, port, msg))