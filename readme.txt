Student: José de Arimatea Rocha Neto
Studen Number: 1001062814

1 - Your header should include all necessary fields, no extraneous fields, and be space efficient. - OK
2 - Running as a client, your protocol should be able to connect to an STCP server and should raise appropriate exceptions. - OK
3 - Running as a client, your protocol should be able to recover from corrupt and dropped segments. - OK
4 - Running as a client, your protocol should be able to have multiple in-flight messages. - NO
5 - Running as a client, your protocol should respect the window size of the server. - NO
6 - Running as a server, your protocol should ACK segments appropriately. - OK
7 - Running as a server, your protocol should support in-order delivery of data from a client, dropping only segments that are outside of the specified window. - OK
8 - Running as a client, your protocol should support the receiving of data from the server (full-duplex). - NO
9 - As a client or server, your protocol should gracefully handle closing (or a closed) connection. - OK
10 - Code style and design (modularity, commenting, error checking, readability) and the clarity of the description of your header format is worth the final 3 marks.- OK