import signal
import sys

# WARNING: This example uses the signal module. In general, a full 
# implementation of signal is available on linux/unix -- but not windows.

# WARNING: Be careful combining signals and threads. In Python, only the main
# thread receives signals, and only the main thread can set signal handlers!
# If you use multiprocessing, instead of threading, you can avoid these issues.

def alarm_handler(signal_num, frame):
    '''Print a message and exit on receipt of a signal.'''

    print("Signal %d received. Exiting." % (signal_num))
    sys.exit(0)


if __name__ == "__main__":
    # Setting a handler for the SIGALRM signal. The second parameter can be any
    # function you wish to be invoked when the alarm is received.
    signal.signal(signal.SIGALRM, alarm_handler)

    # Creating a 10 second timer. itimer is used instead of alarm, as alarm 
    # only accepts integer arguments. For very short timers, itimer is needed.
    prev_delay, prev_interval = signal.setitimer(signal.ITIMER_REAL, 10)

    while 1:
        text = input("Please enter a message: ")
        print("Time remaining: %.2f, Interval: %.2f" % \
              signal.getitimer(signal.ITIMER_REAL))
        print("\tMessage received: %s" % text)

    # The timer can be disabled.
    print("Disabling alarm and exiting.")
    signal.alarm(0)